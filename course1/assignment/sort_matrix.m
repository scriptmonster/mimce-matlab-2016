
% Create an unsorted 5x5 matrix with random integers between 0-100
unsorted_matrix = randi(100, 5, 5);

% Example iteration of the matrix. Every iteration will show corresponding row.
for value = unsorted_matrix
    disp(value)
end;

% convert 5x5 matrix to 1x25 horizontal vector
unsorted_vector = reshape(unsorted_matrix, 1, 25);

% Sort row vector
sorted_vector = sort(unsorted_vector);

% Reshape sorted vector to 5x5 matrix
sorted_matrix = reshape(sorted_vector, 5, 5)';

% Show unsorted and sorted matrixes
disp(unsorted_matrix);
disp(sorted_matrix);

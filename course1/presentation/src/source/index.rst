MATLAB'a Giriş
==============

.. revealjs:: MATLAB'a Giriş
 :title-heading: h2
 :subtitle: Ders 1
 :subtitle-heading: h3

 .. rv_small::

  Sunum: `İsmail Taha AYKAÇ <mailto:ismailtaha@gmail.com>`_

  Kaynaklar: `Bitbucket <https://bitbucket.org/scriptmonster/mimce-matlab-2016>`_

.. revealjs::

 .. revealjs:: Sunum Akışı

  .. rst-class:: fragment

  * **MATLAB Nedir?**
  * MATLAB Ekranı
  * Değişkenler, Listeler, Matrix ve İndeksleme
  * Operatörler
  * Görsel Araçlar
  * Akış Kontrolü
  * M-File Oluşturma

.. revealjs::

 .. revealjs:: MATLAB Nedir?

  .. rst-class:: fragment

   * Bir araçtır.
   * Sizin yapacağınız işi yapmaz.
   * Mühendislik hesaplamalarımızı kolay yapmamızı sağlar.

 .. revealjs:: MATLAB Nedir?

  .. rst-class:: fragment

   * Açılımı **MATrix LABoratory**
   * MATLAB kısaca söylemek gerekirse *Yüksek Seviye Bir Programlama Dili*'dir.
   * Birçok **toolbox** içerir. `bknz <https://www.mathworks.com/products/>`_
   * Çok farklı sektörler için toolbox'lar mevcuttur.


 .. revealjs:: MATLAB Ne Kadar Kullanılıyor

  .. rst-class:: fragment

   .. image:: _static/images/tiobe_index.png

   Kaynak: `Tiobeweb <http://www.tiobe.com/tiobe-index/>`_

.. revealjs:: MATLAB Ekranı

  .. image:: _static/images/matlab_screen.png

.. revealjs:: Değişkenler
 :data-markdown:

 * Bir takım dillerdeki gibi tip tanımına gerek yok!

 ```
  int numeric_variable = 5;
  float floating_number = 10.23;
  char char_variable = 'k';
 ```

 ```
  numeric_variable = 5;
  floating_number = 10.23;
  char_variable = 'k';
 ```
 * Tüm değerler aksi ifade edilmediği sürece **Double** tipinde, **Matris** olarak oluşturulurlar.


.. revealjs::

 .. revealjs:: Array, Matrix
  :data-markdown:

  Vektör (Array)

  ```
   >> vector_example = [1 4 5 3]

   vector_example =

        1     4     5     3
  ```

  Matrix

  ```
   >> matrix_example = [0 5 7 1; 1 0 7 1; 1 4 5 3]

   matrix_example =

        0     5     7     1
        1     0     7     1
        1     4     5     3
  ```
  Transpose

  ```
   >> matris_example'

   ans =

        0     1     1
        5     0     4
        7     7     5
        1     1     3
  ```

 .. revealjs:: Array, Matrix
  :data-markdown:

  Örnekler Devam

  ```
   >> vector_example = 1:10

   vector_example =

        1     2     3     4     5     6     7     8     9    10
  ```

  ```
   >> vector_example = 2:-0.5:-1

   vector_example =

        2.0000    1.5000    1.0000    0.5000         0   -0.5000   -1.0000
  ```

  ```
   >> matrix_example = [1:4; 5:8]

   matrix_example =

        1     2     3     4
        5     6     7     8
  ```

 .. revealjs:: Array, Matrix
  :data-markdown:

  Metodlar Kullanarak Aray ve Matrix Üretme

  ```
   >> vector_example = ones(1, 7)

   vector_example =

        1     1     1     1     1     1     1
  ```

  ```
   >> matrix_example = zeros(4, 2)

   matrix_example =

        0     0
        0     0
        0     0
        0     0
  ```

  ```
   >> matrix_example = rand(2)

   matrix_example =

       0.2238    0.2551
       0.7513    0.5060
  ```

.. revealjs::

 .. revealjs:: İndeksler
  :data-markdown:

  Matrixlerin ilgili elemanlarına erişmek için indeksleme kullanılır.

  ```
   >> matrix_example = randi(100,3,3)

   matrix_example =

       44    80    45
       39    19    65
       77    49    71

   >> matrix_example(0)
   Subscript indices must either be real positive integers or logicals.

   >> matrix_example(1)

   ans =

       44

   >> matrix_example(2)

   ans =

       39

   >> matrix_example(3)

   ans =

       77

   >> matrix_example(4)

   ans =

       80

   >> matrix_example(9)

   ans =

       71

   >>
  ```

 .. revealjs:: İndeksler
  :data-markdown:

  Matrixlerin ilgili elemanlarına erişmek için indeksleme kullanılır.

  ```
   >> matrix_example = randi(100,3,3)

   matrix_example =

       76    66    50
       28    17    96
       68    12    35

   >> matrix_example(1,1)

   ans =

       76

   >> matrix_example(1,2)

   ans =

       66

   >> matrix_example(2,1)

   ans =

       28

   >> matrix_example(2,3)

   ans =

       96

   >> matrix_example(3,3)

   ans =

       35

   >> matrix_example(3,4)
   Index exceeds matrix dimensions.

   >>
  ```


.. revealjs::

 .. revealjs:: Operatörler

  * \+ Toplama
  * \- Çıkarma
  * \* Çarpma
  * \/ Bölme
  * \^ Üs
  * \‘ Transpoz

 .. revealjs:: Eleman Bazlı Operatörler

  * .\* Eleman Bazlı Çarpma
  * .\/ Eleman Bazlı Bölme
  * .\^ Eleman Bazlı Üs

 .. revealjs:: Mantıksal Operatörler

  * \=\= Eşittir
  * \~\= Eşit Değildir
  * \< Küçüktür
  * \> Büyüktür
  * \<\= Küçük Eşittir
  * \>\= Büyük Eşittir
  * \&  Ve operatörü
  * \| Veya operatörü

.. revealjs:: Görsel Araçlar

  * plot
  * stem
  * mesh

.. revealjs:: Akış Kontrolü

 * if
 * for
 * while
 * break

.. revealjs:: M-File Oluşturma

  Birlikte yapalım mı?


.. revealjs:: Bitti :)
 :title-heading: h2
 :subtitle-heading: h3
 :subtitle: Teşekkürler


 .. rv_small::

  Sunum: `İsmail Taha AYKAÇ <mailto:ismailtaha@gmail.com>`_

  Kaynaklar: `Bitbucket <https://bitbucket.org/scriptmonster/mimce-matlab-2016>`_
vector_to_be_sorted = randi(100, 1, 10);

vector_length = size(vector_to_be_sorted, 2);

plot(vector_to_be_sorted, 'o', 'MarkerSize', 40);

count = 0;

for i = vector_length:-1:1

    for index = 1:i - 1
        count = 1 + count;
        left_value = vector_to_be_sorted(index);
        right_value = vector_to_be_sorted(index+1);

        if left_value > right_value
            vector_to_be_sorted(index) = right_value;
            vector_to_be_sorted(index+1) = left_value;
        end

        pause(0.1);
        plot(vector_to_be_sorted, '.', 'MarkerSize', 40);
        disp(count);
    end

end


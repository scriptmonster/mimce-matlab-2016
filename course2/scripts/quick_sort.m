clear;
clc;

vector_to_be_sorted = randi(100, 1, 100);

vector_length = size(vector_to_be_sorted, 2);

% first partition is the whole vector except pivot value.
partitions = [[1 vector_length]];

plot(vector_to_be_sorted, '.', 'MarkerSize', 20);

while size(partitions, 1) > 0
   % abreviation for lower_index li, upper_index ui.
   part_li = partitions(1,1);
   part_ui = partitions(1,2);
  
   % get partition only
   part_vector = vector_to_be_sorted(part_li:part_ui);

   li = 1;
   ui = size(part_vector, 2);
   
   % pivot is the last element of the partition.
   pivot_value = part_vector(ui);
   
   % remove pivot from partition not to iterate.
   part_vector(ui) = [];
   
   % correct upper index.
   ui = ui - 1;
   
   if li == ui
       pivot_index = 1;
       if part_vector(li) < pivot_value
           pivot_index = 2;
       end
   else
       
       while li ~= ui
           while li <= ui && part_vector(li) <= pivot_value
               li = li + 1;
           end

           while li <= ui && part_vector(ui) >= pivot_value
               ui = ui - 1;
           end

           if li == ui || li > ui
               break
           end

           left_value = part_vector(li);
           right_value = part_vector(ui);

           part_vector(ui) = left_value;
           part_vector(li) = right_value;
       end
       pivot_index = li;
   end
    
   part_vector = [part_vector(1:pivot_index-1) pivot_value part_vector(pivot_index:size(part_vector, 2))];
   
   
   vector_to_be_sorted(part_li:part_ui) = part_vector;
   
   partitions(1, :) = [];
   
   if part_li+pivot_index < part_ui
       partitions = [
           part_li+pivot_index part_ui;
           partitions
       ];
   end
   
   if part_li < part_li+pivot_index-2
       partitions = [
           part_li part_li+pivot_index-2;
           partitions
       ];
   end
   
   plot(vector_to_be_sorted, '.', 'MarkerSize', 20);
   pause(1)
   
   
end

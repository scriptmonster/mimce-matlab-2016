clear;
clc;

% integer random alternative
%vector_to_sort = randi(100, 1, 1000);

% Random vector, which will be sorted below
vector_to_sort = rand(1, 1000);
first_vector = vector_to_sort;


vector_length = size(vector_to_sort, 2);

for idx = 1:vector_length
    for inner_idx = 1:vector_length-1

        if vector_to_sort(inner_idx) > vector_to_sort(inner_idx+1)
            left_value = vector_to_sort(inner_idx);
            right_value = vector_to_sort(inner_idx+1);

            vector_to_sort(inner_idx) = right_value;
            vector_to_sort(inner_idx+1) = left_value;
        end

    end
end

plot(vector_to_sort, '.', 'MarkerSize', 20);
hold on;
plot(first_vector, 'r.', 'MarkerSize', 20);
hold off;
